#!/bin/sh

# install podman
dnf -y install podman

# pull the jellyfin container
podman pull docker.io/jellyfin/jellyfin

# stop jellyfin delete jellyfin container
systemctl stop jellyfin
podman rm jellyfin

# create new jellyfin container
podman create --label "io.containers.autoupdate=image" --cgroup-manager=systemd --privileged --volume jellyfin-config:/config --volume jellyfin-cache:/cache --volume jellyfin-media:/media --net=host --name=jellyfin docker.io/jellyfin/jellyfin

# generate systemd service 
podman generate systemd --name jellyfin > /etc/systemd/system/jellyfin.service
systemctl daemon-reload

systemctl enable --now jellyfin